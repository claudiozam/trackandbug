package edu.curso.java.bo;

import java.util.*;

import javax.persistence.*;

@Entity
public class Proyecto {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String nombre;
	private String descripcion;
	private Date fechaDeAlta;
	
	@ManyToOne
	private EstadoProyecto estadoProyecto;
	
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Date getFechaDeAlta() {
		return fechaDeAlta;
	}
	
	public void setFechaDeAlta(Date fechaDeAlta) {
		this.fechaDeAlta = fechaDeAlta;
	}
	
	public EstadoProyecto getEstadoProyecto() {
		return estadoProyecto;
	}
	
	public void setEstadoProyecto(EstadoProyecto estadoProyecto) {
		this.estadoProyecto = estadoProyecto;
	}
	
	
}
